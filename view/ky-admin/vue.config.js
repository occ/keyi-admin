const path = require('path') 
const { config } = require('process')
const port = process.env.port || process.env.npm_config_port || 8282
const name="柯一后台管理系统"
function resolve(dir) {
    return path.join(__dirname, dir)
}
module.exports={
	lintOnSave:false,
	publicPath:process.env.NODE_ENV === "production" ? "/":"/",
	outputDir:'dist',
	assetsDir:'static',
	productionSourceMap:false,
	devServer:{
		host:'localhost',
		port:port,
		open:true,
		
		
	},
	chainWebpack:config=>{
		 config.resolve.symlinks(true);
		 config.resolve.alias.set('@',resolve('src'))
		 config.plugin('html').tap(options=>{
		     options[0].title=name
		     return options
		 })
	}
}

