import request from '@/common/request'

export function listDatas(data) {
  return request({
    url: '/datas/index',
    method: 'post',
	data:data
  })
}