keyiAdmin 1.0
===============

本版本已到2.0，然后没发布在这个上面，放在QQ群文件
请加QQ群，下载 QQ群号：781216188

> 运行环境要求PHP7.1+。

> VUECLI4.4+。

## 使用框架

> thinkphp6.0。

> vue2.0。

> vuecli4.0。

> element ui。

## 目录结构
~~~
前端目录 view/ky-admin
~~~
~~~
数据文件目录：data/keyi.sql
~~~
## 主要功能
* 系统管理
* 文件管理
* 数据管理
* 代码生成(开发中..)




## 运行

~~~

1. 将代码部署到本地或者服务器
2. 修改ky-admin前端的接口地址，地址目录view/ky-admin/.env文件
~~~

```
VUE_APP_PUBLIC_PATH = "http://127.0.0.1:8000"  //将这里换成你自己的访问路径
VUE_APP_API = "http://127.0.0.1:8000/apiadmin/"
```
~~~
3. 将ky-admin用编辑打开
~~~


```
用命令执行
npm install
npm run dev
```
~~~
4. 正式发布
~~~

```
npm run build //前端打包

```
## 版权信息

遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

## 项目截图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0112/220803_f5831f0b_1091193.png "1.png")


![输入图片说明](https://images.gitee.com/uploads/images/2021/0112/220955_3eb8a1aa_1091193.png "2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0112/221003_94f7cad0_1091193.png "3.png")


![输入图片说明](https://images.gitee.com/uploads/images/2021/0112/221013_76845ac9_1091193.png "4.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0112/221022_1b9a9a29_1091193.png "5.png")
